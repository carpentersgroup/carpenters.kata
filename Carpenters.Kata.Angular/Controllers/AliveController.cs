﻿using System.Web.Http;

namespace Carpenters.Kata.Angular.Controllers
{
    [Route("")]
    public class AliveController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok("Hello World!");
        }
    }
}