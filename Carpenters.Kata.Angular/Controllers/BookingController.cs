﻿using Carpenters.Kata.Angular.Models;
using Carpenters.Kata.Angular.Services;
using Carpenters.Kata.Angular.Services.Interfaces;
using System.Collections.Generic;
using System.Web.Http;

namespace Carpenters.Kata.Angular.Controllers
{
    /// <summary>
    /// Your Name:
    /// Date: 
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    [RoutePrefix("booking")]
    public class BookingController : ApiController
    {
        private readonly IBookingService bookingService;

        public BookingController() : this (new BookingService())
        {
        }

        public BookingController(IBookingService bookingService)
        {
            this.bookingService = bookingService;
        }

        [HttpGet]
        [Route("")]        
        public IHttpActionResult Get()
        {
            IEnumerable<Booking> bookings = bookingService.GetAllBookings();

            return Ok(bookings);
        }
    }
}