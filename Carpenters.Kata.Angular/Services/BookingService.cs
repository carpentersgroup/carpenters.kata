﻿using Carpenters.Kata.Angular.Models;
using Carpenters.Kata.Angular.Services.Interfaces;
using System.Collections.Generic;

namespace Carpenters.Kata.Angular.Services
{
    public class BookingService : IBookingService
    {
        private static IList<Booking> bookings;

        public BookingService() : this(new DataService())
        {
        }

        public BookingService(IDataService dataService)
        {
            bookings = dataService.Initialize();
        }

        public IList<Booking> GetAllBookings()
        {
            return bookings;
        }

        public void CreateBooking(Booking booking)
        {
        }
    }
}